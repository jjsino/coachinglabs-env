# coachinglabs-env

This repository is supposed to provide a DEV environment (php5.4, mysql5.5, apache2.0) with docker.

No sensitive information should be included here.

## Installation lebootcamp

* checkout env settings repo

``` git clone git@bitbucket.org:jjsino/coachinglabs-env.git [MY_WORKSPACE] ```


- go to the directory

``` cd /[path_to]/[MY_WORKSPACE] ```


- checkout lebootcamp project from svn

``` svn checkout [REPO_SVN]/trunk/top lebootcamp ```

- create docker volume for data_mysql
``` docker volume create --name=data_mysql --opt device=[PATH_TO]/[MY_WORKSPACE]/data ```

- add ``` 127.0.0.1  dev.lebootcamp.com ``` to /etc/hosts
 